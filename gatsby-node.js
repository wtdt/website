const path = require("path")
const { linkResolver } = require("./src/utils/linkResolver")

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions

  const pagesFromData = (edges, template) => {
    edges.forEach(edge => {
      createPage({
        path: linkResolver(edge.node),
        component: template,
        context: {
          uid: edge.node.uid
        }
      })
    })
  }

  const episodeData = await graphql(`
    {
      allPrismicEpisode {
        edges {
          node {
            id
            uid
            type
          }
        }
      }
    }
  `)
  pagesFromData(
    episodeData.data.allPrismicEpisode.edges,
    path.resolve("src/templates/episode.js")
  )

  const teamData = await graphql(`
    {
      allPrismicTeamMember {
        edges {
          node {
            id
            uid
            type
          }
        }
      }
    }
  `)
  pagesFromData(
    teamData.data.allPrismicTeamMember.edges,
    path.resolve("src/templates/member.js")
  )
}
