require("dotenv").config()
const { resolveLink } = require("./src/utils/linkResolver.js")
const path = require("path")

module.exports = {
  siteMetadata: {
    title: `Why They Did That`,
    description: `Why They Did That`,
    author: `Caleb Whiting`
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `The Why They Did That Podcast`,
        short_name: `WTDT`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#F0D24B`,
        display: `minimal-ui`,
        icon: `src/images/wtdt-square.png`
      }
    },
    `gatsby-plugin-postcss`,
    {
      resolve: "gatsby-plugin-purgecss",
      options: {
        tailwind: true,
        purgeOnly: ["src/css/style.css"]
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: path.join(__dirname, `src`, `images`)
      }
    },
    {
      resolve: `gatsby-plugin-sharp`,
      options: {
        useMozJpeg: true,
        defaultQuality: 75
      }
    },
    `gatsby-transformer-sharp`,
    // {
    //   resolve: "gatsby-source-prismic-graphql",
    //   options: {
    //     repositoryName: "wtdt", // (required)
    //     accessToken: process.env.PRISMIC_ACCESS_TOKEN,
    //     path: "/preview", // (optional, default: /preview)
    //     previews: true,
    //     omitPrismicScript: true,
    //     pages: [
    //       {
    //         type: "Episode", // TypeName from prismic
    //         match: "/episode/:uid", // Pages will be generated under this pattern (optional)
    //         path: "/preview_episode", // Placeholder page for unpublished documents
    //         component: require.resolve("./src/templates/episode.js")
    //       },
    //       {
    //         type: "Team_member",
    //         match: "/member/:uid",
    //         path: "/preview_member",
    //         component: require.resolve("./src/templates/member.js")
    //       }
    //     ]
    //   }
    // },
    {
      resolve: `gatsby-source-prismic`,
      options: {
        repositoryName: `wtdt`,
        accessToken: `${process.env.PRISMIC_ACCESS_TOKEN}`,
        linkResolver: resolveLink
      }
    },
    `gatsby-plugin-subfont`
  ]
}
