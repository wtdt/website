import React from "react"
import { graphql } from "gatsby"
import clsx from "clsx"
import Img from "gatsby-image"
import Link from "gatsby-link"
import SEO from "../components/seo"

import Layout, { globalPadding } from "../components/Layout"
import {
  facebook,
  twitter,
  instagram,
  globe,
  linkedin,
  backChevron,
  LinkIcon,
  Icon
} from "../elements/icons"

export const query = graphql`
  query MemberQuery($uid: String) {
    prismicTeamMember(uid: { eq: $uid }) {
      uid
      id
      type
      data {
        name {
          text
        }
        role {
          text
        }
        bio {
          text
        }
        picture {
          alt
          localFile {
            childImageSharp {
              fluid(maxWidth: 800) {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
          }
        }
        linkedin {
          link_type
          url
          target
        }
        instagram {
          link_type
          url
          target
        }
        personal_website {
          link_type
          url
          target
        }
        facebook_page {
          link_type
          url
          target
        }
        twitter {
          link_type
          url
          target
        }
      }
    }
  }
`

const Episode = ({ data }) => {
  const node = data.prismicTeamMember

  return (
    <Layout>
      <SEO
        title={node.data.name.text}
        keywords={[node.data.role.text, `employee`, `volunteer`, `member`]}
      />
      <div className={clsx("flex flex-col items-center mb-12", globalPadding)}>
        {/* limit max width */}
        <div className="w-full max-w-6xl">
          {/* text | picture */}
          <div className="flex flex-col md:flex-row items-start xl:items-center mb-12 mt-12">
            {/* text */}
            <div className="flex flex-col items-start relative">
              <Link
                to="/team"
                className="text-blue-500 text-md md:text-lg pb-4 flex items-center absolute "
                style={{ top: "-3rem" }}
              >
                <Icon src={backChevron} className="h-4 md:h-6" />
                All team members
              </Link>
              <div
                className="flex flex-col p-6 md:mr-8 mb-8 md:mb-0"
                style={{ background: "#f0f0f0" }}
              >
                <h2 className="font-semibold  mb-4 text-2xl sm:text-3xl lg:text-4xl">
                  {node.data.role.text}: {node.data.name.text}
                </h2>
                <div className="text-gray-700 mb-8">{node.data.bio.text}</div>
                <div className="flex">
                  {node.data.facebook_page && node.data.facebook_page.url && (
                    <SocialIcon
                      href={node.data.facebook_page.url}
                      title="Their Facebook page"
                      src={facebook}
                    />
                  )}
                  {node.data.twitter && node.data.twitter.url && (
                    <SocialIcon
                      href={node.data.twitter.url}
                      title="Their Twitter profile"
                      src={twitter}
                    />
                  )}
                  {node.data.instagram && node.data.instagram.url && (
                    <SocialIcon
                      href={node.data.instagram.url}
                      title="Their Instagram account"
                      src={instagram}
                    />
                  )}
                  {node.data.linkedin && node.data.linkedin.url && (
                    <SocialIcon
                      href={node.data.linkedin.url}
                      title="Their linkedin profile"
                      src={linkedin}
                    />
                  )}
                  {node.data.personal_website &&
                    node.data.personal_website.url && (
                      <SocialIcon
                        href={node.data.personal_website.url}
                        title="Their personal website"
                        src={globe}
                      />
                    )}
                </div>
              </div>
            </div>
            {/* picture */}
            {node.data.picture.localFile && (
              <div className="flex-none rounded overflow-hidden w-92 md:w-64 lg:w-92 max-w-full">
                <Img
                  alt={node.data.picture.alt}
                  className="w-full h-full object-cover object-center"
                  fluid={node.data.picture.localFile.childImageSharp.fluid}
                />
              </div>
            )}
          </div>
        </div>
      </div>
    </Layout>
  )
}

const SocialIcon = ({ title, href, src, className }) => {
  return (
    <LinkIcon
      title={title}
      href={href}
      src={src}
      className={clsx(
        "h-10 mr-4 text-gray-700 hover:text-blue-500 transition-010",
        className
      )}
    />
  )
}

export default Episode
