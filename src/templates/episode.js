import React from "react"
import Img from "gatsby-image/withIEPolyfill"
import { graphql } from "gatsby"
import { format } from "date-fns"
import clsx from "clsx"
import SEO from "../components/seo"

import { linkResolver } from "../utils/linkResolver"
import Layout, { globalPadding } from "../components/Layout"

export const query = graphql`
  query EpisodeQuery($uid: String!) {
    prismicEpisode(uid: { eq: $uid }) {
      uid
      id
      type
      data {
        name {
          text
        }
        description {
          text
          html
        }
        guest {
          text
        }
        bible_character {
          text
        }
        release_date
        length {
          text
        }
        cover_image {
          alt
          copyright
          url
          localFile {
            childImageSharp {
              fluid(maxWidth: 368) {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
          }
        }
        audio_url {
          url
          target
        }
        transistor_id
      }
    }
  }
`

const Episode = props => {
  const doc = props.data.prismicEpisode
  console.log(doc)

  return (
    <Layout>
      <SEO
        title={doc.data.name.text}
        keywords={[
          `episode`,
          `wtdt`,
          doc.data.name.text,
          doc.data.bible_character.text,
          doc.data.guest.text
        ]}
      />
      <div className={clsx("flex flex-col items-center mb-12", globalPadding)}>
        {/* limit max width */}
        <div className="max-w-6xl">
          {/* picture | text */}
          <div className="flex items-start flex-col md:flex-row xl:items-center mb-12">
            {/* picture */}
            <div className="flex-none bg-yellow-450 rounded md:mr-8 overflow-hidden mb-8 md:mb-0 w-64 h-64 lg:w-92 lg:h-92">
              <Img
                className="w-full h-full object-cover object-center"
                fluid={doc.data.cover_image.localFile.childImageSharp.fluid}
                alt={doc.data.cover_image.alt}
              />
            </div>
            {/* text */}
            <div
              className="flex flex-col p-6"
              style={{ background: "#f0f0f0" }}
            >
              <h2 className="font-semibold text-4xl mb-4">
                {doc.data.bible_character.text}
                {doc.data.bible_character ? ": " : null}
                {doc.data.name.text}
              </h2>
              <div
                className="text-gray-700 mb-8"
                dangerouslySetInnerHTML={{ __html: doc.data.description.html }}
              ></div>
              <div className="su-table su-table-alternate">
                <table className="">
                  <tbody>
                    <Row title="GUEST" value={doc.data.guest.text} />
                    <Row
                      title="BIBLICAL CHARACTER"
                      value={doc.data.bible_character.text}
                    />
                    <Row
                      title="RELEASE DATE"
                      value={format(
                        new Date(doc.data.release_date),
                        "MMMM D, YYYY"
                      )}
                    />
                    <Row title="LENGTH" value={doc.data.length.text} />
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <iframe
            src={`https://share.transistor.fm/e/${doc.data.transistor_id}`}
            width="100%"
            height="180"
            frameBorder="0"
            scrolling="no"
            seamless
            style={{ width: "100%, height: 180px" }}
          ></iframe>

          {/* <BlubrryPlayer
            media_url={doc.data.audio_url.url}
            cover_url={doc.data.cover_image.url}
            // TODO: convert to absolute url
            podcast_link={linkResolver(doc)}
          /> */}
        </div>
      </div>
    </Layout>
  )
}

export default Episode

const Row = ({ title, value }) => {
  return (
    <tr>
      <td>
        <h5>{title}</h5>
      </td>
      <td>{value}</td>
    </tr>
  )
}

const BlubrryPlayer = ({ media_url, cover_url, podcast_link }) => {
  const query = {
    media_url: media_url,
    podcast_link: podcast_link,
    artwork_url: cover_url
  }
  let queryArray = []
  Object.keys(query).forEach(key => {
    queryArray.push(`${key}=${encodeURIComponent(query[key])}`)
  })
  const queryString = queryArray.join("&")
  const url = `https://player.blubrry.com/?${queryString}#darkOrLight-light&shownotes-ffffff&shownotesBackground-318ccc&download-ffffff&downloadBackground-147fcc&subscribe-ffffff&subscribeBackground-318ccc&share-ffffff&shareBackground-147fcc`

  return (
    <iframe
      src={url}
      scrolling="no"
      width="100%"
      height="138px"
      frameBorder="0"
      id="blubrryplayer-4"
    />
  )
}
