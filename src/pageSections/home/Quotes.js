import React from "react"
import clsx from "clsx"
import { graphql, StaticQuery } from "gatsby"
import "typeface-libre-baskerville"

import { globalPadding } from "../../components/Layout"

const Quotes = () => {
  return (
    <StaticQuery
      query={graphql`
        {
          testimonials: allPrismicHomepage {
            edges {
              node {
                data {
                  body {
                    primary {
                      quote {
                        html
                        text
                      }
                      attribution {
                        html
                        text
                      }
                    }
                  }
                }
              }
            }
          }
        }
      `}
      render={data => {
        const testimonials = data.testimonials.edges[0].node.data.body

        return (
          <div className="bg-white flex justify-center">
            <div
              className={clsx(
                globalPadding,
                "mt-4 sm:mt-32 mb-8 sm:mb-16 max-w-6xl w-full"
              )}
            >
              {testimonials.map((t, i) => {
                return (
                  <Quote
                    alignRight={i % 2}
                    attribution={t.primary.attribution}
                    quote={t.primary.quote}
                    key={t.primary.attribution.text + t.primary.quote.text}
                  />
                )
              })}
            </div>
          </div>
        )
      }}
    />
  )
}

export default Quotes

const Quote = ({ quote, attribution, alignRight = false }) => {
  return (
    <div
      className={clsx(
        "flex flex-col sm:flex-row max-w-2xl mb-4 sm:mb-16",
        alignRight ? "ml-auto" : "mr-auto"
      )}
    >
      {/* quote header */}
      <div className="flex justify-start pb-6 sm:pr-6 small-quote md:no-small-quote">
        {/* quote icon */}
        <div
          className="ml-8 w-24 h-24 rounded-full bg-yellow-450"
          //   style={{ marginLeft: "46px", marginBottom: "23px" }}
        >
          <svg
            className="relative w-32 h-32"
            style={{
              right: "4.7rem",
              top: "-0.2rem",
              width: "10rem",
              height: "10rem"
            }}
            xmlns="http://www.w3.org/2000/svg"
            width="32"
            height="32"
            viewBox="0 0 32 32"
          >
            <path d="M8 13.12a3 3 0 0 0 2.94 3.06c.53 0 1-.15 1.43-.4-.14 1.63-.97 4.02-3.99 6.45-.42.34-.5.98-.17 1.41a.94.94 0 0 0 1.36.18c3.7-2.99 4.63-6.06 4.76-8.11.21-2.63-.59-4-1.31-4.73l-.2-.2a3.07 3.07 0 0 0-.47-.33l-.02-.02h-.03c-.2-.12-.34-.17-.34-.17l.01.02a2.69 2.69 0 0 0-1.03-.22A3 3 0 0 0 8 13.12zm9.7 0a3 3 0 0 0 2.94 3.06c.52 0 1-.15 1.43-.4-.15 1.63-.97 4.02-3.99 6.45-.42.34-.5.98-.17 1.41.19.26.48.4.76.4.21 0 .42-.07.6-.22 3.7-2.99 4.63-6.06 4.75-8.11.22-2.63-.58-4-1.3-4.73l-.21-.2a3.07 3.07 0 0 0-.47-.33l-.01-.02H22c-.2-.12-.34-.17-.34-.17v.02a2.7 2.7 0 0 0-1.02-.22 3 3 0 0 0-2.95 3.06z" />
          </svg>
        </div>
      </div>
      {/* quote RHS */}
      <div className="flex flex-col">
        {/* quote text */}
        <div
          className="font-serif text-xl sm:text-2xl mb-8"
          dangerouslySetInnerHTML={{ __html: quote.html }}
        />
        {/* quote attribution */}
        <div className="text-lg sm:text-xl">- {attribution.text}</div>
      </div>
    </div>
  )
}
