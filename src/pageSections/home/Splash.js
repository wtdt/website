import React, { useState, useEffect } from "react"
import { StaticQuery, graphql, Link } from "gatsby"
import clsx from "clsx"
import Img from "gatsby-image/withIEPolyfill"

import { linkResolver } from "../../utils/linkResolver.js"
import { headerSlimHeightExpr } from "../../components/Header"
import { globalPadding } from "../../components/Layout"
import appleBadge from "../../images/listen-on-apple.png"
import spotifyBadge from "../../images/listen-on-spotify.png"
// import bgImg from "../../images/job.svg"

const Splash = () => {
  return (
    <StaticQuery
      query={graphql`
        query {
          episodes: allPrismicEpisode(
            sort: { fields: [data___season, data___episode_number], order: ASC }
          ) {
            edges {
              node {
                uid
                type
                data {
                  illustration {
                    alt
                    copyright
                    url
                    localFile {
                      childImageSharp {
                        fluid(maxWidth: 4096) {
                          ...GatsbyImageSharpFluid_withWebp_noBase64
                        }
                      }
                    }
                  }
                  transistor_id
                  season
                  episode_number
                }
              }
            }
          }
        }
      `}
      render={data => <SplashWithData data={data} />}
    />
  )
}

const SplashWithData = ({ data }) => {
  console.log(data)
  let latestEpisode
  data.episodes.edges.forEach(edge => {
    let currentEpisode = edge.node.data

    if (latestEpisode) {
      if (latestEpisode.season < currentEpisode.season) {
        latestEpisode = currentEpisode
      } else if (
        latestEpisode.season === currentEpisode.season &&
        latestEpisode.episode_number < currentEpisode.episode_number
      ) {
        latestEpisode = currentEpisode
      }
    } else {
      latestEpisode = currentEpisode
    }
  })

  return (
    <div
      className="bg-yellow-450 flex p-16 justify-center relative"
      style={{ paddingTop: headerSlimHeightExpr, minHeight: "75vh" }}
    >
      <div
        className="absolute w-full md:w-2/3 h-full top-0 right-0"
        style={{ opacity: "0.5" }}
      >
        <Img
          fluid={latestEpisode.illustration.localFile.childImageSharp.fluid}
          className="w-full h-full"
        />
      </div>
      <div className="max-w-6xl flex items-center flex-col md:flex-row relative">
        <div className="flex md:w-1/2 flex flex-col pb-10 md:pb-0 md:pr-8 items-start">
          <h1
            className="font-medium text-5xl mb-2"
            style={{ textShadow: "1px 1px 2px #eed348" }}
          >
            Season 2 is out!
          </h1>
          <h2
            className="text-lg mb-8 "
            style={{ textShadow: "1px 1px 2px #eed348" }}
          >
            Why They Did That is a podcast that explores the motivations of
            biblical characters and how their choices can guide yours.
          </h2>
          <div className="flex flex-col sm:flex-row md:flex-col">
            <a href="https://podcasts.apple.com/us/podcast/why-they-did-that/id1434005288">
              <img
                className="mb-6 sm:mr-8 sm:mb-0 md:mb-4 md:m4-0"
                src={appleBadge}
              />
            </a>
            <a
              href="https://open.spotify.com/show/1FdElWb8fAlmvPxy59e3uv"
              target="_blank"
            >
              <img src={spotifyBadge} />
            </a>
          </div>
        </div>
        <div className="w-full md:w-1/2 md:pl-8">
          <iframe
            src={`https://share.transistor.fm/e/${latestEpisode.transistor_id}/dark`}
            width="100%"
            height="180"
            frameBorder="0"
            scrolling="no"
            seamless
            style={{ width: "100%", height: "180px" }}
          ></iframe>
        </div>
      </div>
    </div>
  )
}

export default Splash
