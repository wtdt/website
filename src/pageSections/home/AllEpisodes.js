import React, { useState, useEffect } from "react"
import { StaticQuery, graphql, Link } from "gatsby"
import clsx from "clsx"
import Img from "gatsby-image/withIEPolyfill"

import { linkResolver } from "../../utils/linkResolver.js"
import { headerSlimHeightExpr } from "../../components/Header"
import { globalPadding } from "../../components/Layout"
// import bgImg from "../../images/job.svg"

const Splash = () => {
  return (
    <StaticQuery
      query={graphql`
        query {
          episodes: allPrismicEpisode(
            sort: { fields: [data___season, data___episode_number], order: ASC }
          ) {
            edges {
              node {
                uid
                type
                data {
                  name {
                    html
                    text
                  }
                  illustration {
                    alt
                    copyright
                    url
                    localFile {
                      childImageSharp {
                        fluid(maxWidth: 4096) {
                          ...GatsbyImageSharpFluid_withWebp_noBase64
                        }
                      }
                    }
                  }
                  episode_number
                  season
                }
              }
            }
          }
        }
      `}
      render={data => <SplashWithData data={data} />}
    />
  )
}

const SplashWithData = ({ data }) => {
  const episodes = data.episodes.edges

  const seasonOneEpisodes = episodes.filter(edge => edge.node.data.season === 1)
  const seasonTwoEpisodes = episodes.filter(edge => edge.node.data.season === 2)

  let [season, setSeason] = useState(1)

  return (
    <div className={clsx(globalPadding, "flex flex-col items-start")}>
      <h1 className="text-4xl font-semibold pb-4 pt-8">All Episodes</h1>

      <div className="flex">
        <SeasonTitle season={1} currentSeason={season} setSeason={setSeason} />

        {seasonTwoEpisodes.length ? (
          <SeasonTitle
            season={2}
            currentSeason={season}
            setSeason={setSeason}
          ></SeasonTitle>
        ) : null}
      </div>

      <EpisodesGrid
        episodes={season == 1 ? seasonOneEpisodes : seasonTwoEpisodes}
      />
    </div>
  )
}

export default Splash

const SeasonTitle = ({ className, season, currentSeason, setSeason }) => {
  let selected = season === currentSeason

  return (
    <button
      className={clsx(
        "text-xl font-semibold mb-8 p-1 px-2 mx-2 text-gray-800 border-b-4 border-yellow-450",
        { "bg-yellow-450": selected },
        className
      )}
      onClick={() => setSeason(season)}
    >
      Season {season}
    </button>
  )
}

const EpisodesGrid = ({ episodes, className }) => {
  return (
    <div className={clsx("EpisodesGrid w-full", className)}>
      {/* negative margin eats the residual padding from each list item */}
      <div className="flex flex-wrap -m-4">
        {episodes.map(({ node }) => {
          return <PodcastCard key={node.uid} node={node} />
        })}
      </div>
    </div>
  )
}

const PodcastCard = ({ node, onMouseOver, isCurrent }) => {
  return (
    <Link
      to={linkResolver(node)}
      className="w-full sm:h-80 sm:w-1/2 lg:w-1/3 xl:w-1/4 p-4 relative overflow-hidden "
      onMouseOver={onMouseOver}
      onTouchStart={onMouseOver}
    >
      <Img
        fluid={node.data.illustration.localFile.childImageSharp.fluid}
        className={clsx(
          "absolute top-0 left-0 w-full h-full -z-1 transition-home-bg",
          {
            "scale-1": !isCurrent,
            "scale-103": isCurrent
          }
        )}
        style={{ opacity: "50%" }}
      />
      <div
        className={clsx(
          "absolute w-full h-full top-0 left-0 flex flex-col justify-end p-8"
        )}
      >
        <p className="text-gray-700">Episode {node.data.episode_number}</p>
        <h2 className="text-gray-900 font-semibold tracking-tight leading-none text-4xl">
          {node.data.name.text}
        </h2>
      </div>
    </Link>
  )
}

const FadeInImg = ({ src, alt, className }) => {
  const [loaded, setLoaded] = useState(false)
  const img = React.createRef()

  useEffect(() => {
    if (img.current) {
      if (alreadyLoaded(img.current)) {
        setLoaded(true)
      }
    }
  }, [loaded])

  return (
    <img
      src={src}
      alt={alt}
      className={clsx(className, { "opacity-0": !loaded })}
      onLoad={() => setLoaded(true)}
      ref={img}
    />
  )
}

const alreadyLoaded = img => {
  // During the onload event, IE correctly identifies any images that
  // weren’t downloaded as not complete. Others should too. Gecko-based
  // browsers act like NS4 in that they report this incorrectly.
  if (!img.complete) {
    return false
  }

  // However, they do have two very useful properties: naturalWidth and
  // naturalHeight. These give the true size of the image. If it failed
  // to load, either of these should be zero.
  if (img.naturalWidth === 0) {
    return false
  }

  // No other way of checking: assume it’s ok.
  return true
}
