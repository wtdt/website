import React from "react"
import clsx from "clsx"
import Link from "gatsby-link"
import { globalPadding } from "../../components/Layout"

const linkClass =
  "bg-blue-wtdt px-8 py-4 font-semibold text-xl md:text-2xl text-white mr-8 md:ml-8 md:mr-0"

const HelpUs = () => {
  return (
    <div className={clsx(globalPadding, "flex flex-col items-center bg-white")}>
      <div className="flex border-t py-16 w-full justify-center border-gray-500">
        <div className="max-w-6xl flex w-full flex-col md:flex-row md:justify-between align-start md:align-center">
          {/* text */}
          <div className="flex flex-col mb-8 md:mb-0">
            <h4 className="font-semibold text-3xl mb-1">WE NEED YOU</h4>
            <p className="text-gray-700 text-lg">
              Show your support by donating your time, talents, or resources.
            </p>
          </div>
          {/* buttons */}
          <div className="flex items-center flex-row">
            <Link to="/help" className={linkClass}>
              HELP
            </Link>
            <a
              href="https://weimar.org/donate/info/why-they-did-that"
              target="_blank"
              className={linkClass}
            >
              DONATE
            </a>
          </div>
        </div>
      </div>
    </div>
  )
}

export default HelpUs
