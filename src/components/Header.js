import { Link } from "gatsby"
import PropTypes from "prop-types"
import React, { useState } from "react"
import clsx from "clsx"

import logo from "../images/logo.svg"
import { globalPadding } from "../components/Layout"

export const headerSlimHeightExpr = `calc(2rem + 120px + 2rem)`

const Header = props => {
  const {
    // Site title
    siteTitle,
    // additional class names to add to the outer
    className,
    // Make the header smaller (160px vs 190px)
    slim,
    // Don't take up any space.  Child content will display below the header.
    float
  } = props

  const [isExpanded, setExpanded] = useState(false)

  return (
    // full-width nav
    <nav
      className={clsx(
        globalPadding,
        "py-8 flex items-center z-10",
        { "absolute top-0 left-0 right-0": float },
        className
      )}
    >
      {/* logo on the left */}
      <Link to="/" className="mr-auto flex">
        <img
          src={logo}
          alt={siteTitle}
          className="object-contain object-center"
          style={{ height: slim ? "120px" : "150px" }}
        />
      </Link>

      {/* expansion button on the right */}
      <button
        className="flex items-center text-gray-800 ml-4"
        onClick={() => setExpanded(true)}
      >
        <span className="mr-2 font-semibold text-sm sm:text-base">MENU</span>
        <svg
          className="fill-current h-6 w-6 sm:h-8 sm:w-8"
          viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg"
        >
          <title>Menu</title>
          <path d="M17.5 6h-15a.5.5 0 0 1 0-1h15a.5.5 0 0 1 0 1zM17.5 11h-15a.5.5 0 0 1 0-1h15a.5.5 0 0 1 0 1zM17.5 16h-15a.5.5 0 0 1 0-1h15a.5.5 0 0 1 0 1z" />
        </svg>
      </button>

      {/* The slide-out navigation */}
      <div
        className={clsx("fixed inset-0 flex", {
          "pointer-events-none": !isExpanded
        })}
      >
        {/* gray overlay */}
        <div
          onClick={() => setExpanded(false)}
          className={clsx("absolute w-full h-full bg-nd cursor-pointer z-20", {
            "opacity-0": !isExpanded
          })}
        />

        {/* white sidebar */}
        <div
          className="h-full bg-white ml-auto pt-12 px-24 z-20"
          style={{
            width: "675px",
            transition: "transform 0.4s ease",
            transform: isExpanded
              ? "translate3d(0, 0, 0)"
              : "translate3d(675px, 0, 0)"
          }}
        >
          {/* close button */}
          <button className="absolute" style={{ top: "50px", right: "50px" }}>
            <svg
              className=" w-12 h-12"
              xmlns="http://www.w3.org/2000/svg"
              width="20"
              height="20"
              viewBox="0 0 20 20"
              onClick={() => setExpanded(false)}
            >
              <path d="M10.7 10.5l5.65-5.65a.5.5 0 0 0-.7-.7L10 9.79 4.35 4.15a.5.5 0 0 0-.7.7l5.64 5.65-5.64 5.65a.5.5 0 0 0 .7.7L10 11.21l5.65 5.64a.5.5 0 0 0 .7 0 .5.5 0 0 0 0-.7l-5.64-5.65z" />
            </svg>
          </button>

          {/* list of links */}
          <ul className="text-4xl font-semibold text-gray-600">
            <NavbarLink setExpanded={setExpanded} to="/">
              Home
            </NavbarLink>
            <NavbarLink setExpanded={setExpanded} to="/team">
              Team
            </NavbarLink>
            <NavbarLink setExpanded={setExpanded} to="/help">
              Help
            </NavbarLink>
            <NavbarLink setExpanded={setExpanded} to="/advertise">
              Advertise
            </NavbarLink>
          </ul>
        </div>
      </div>
    </nav>
  )
}

Header.propTypes = {
  siteTitle: PropTypes.string
}

Header.defaultProps = {
  siteTitle: ``
}

export default Header

const NavbarLink = ({ to, children, setExpanded }) => {
  return (
    <li>
      <Link
        onClick={() => setExpanded(false)}
        to={to}
        activeClassName="text-gray-900"
        className="transition-010 hover:text-gray-900"
      >
        {children}
      </Link>
    </li>
  )
}
