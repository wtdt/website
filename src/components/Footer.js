import React from "react"
import clsx from "clsx"

import { facebook, twitter, instagram, LinkIcon } from "../elements/icons"
import { globalPadding } from "../components/Layout"
import footerLogo from "../images/wtdt-round.png"
import itunesPodcastsLogo from "../images/itunes-podcasts.png"
import spotifyLogo from "../images/spotify.png"
import stitcherLogo from "../images/stitcher.png"

const Footer = () => {
  return (
    <footer className={clsx(globalPadding, "bg-white relative")}>
      <div className="py-16 flex flex-col md:flex-row justify-between items-center border-t border-gray-500">
        <div className="flex flex-col lg:flex-row items-center">
          <img
            className="mb-8 lg:mb-0 lg:mr-6 "
            src={footerLogo}
            alt="Why they did that logo and Weimar logo."
          />
          <div className="flex mb-10 md:mb-0">
            <SocialIcon
              href="https://www.facebook.com/whytheydidthat/"
              title="Why They Did That Facebook"
              src={facebook}
            />
            <SocialIcon
              href="https://twitter.com/WhyTheyDidThat"
              title="Why They Did That Twitter"
              src={twitter}
            />
            <SocialIcon
              href="https://www.instagram.com/whytheydidthat/"
              title="Why They Did That Instagram"
              src={instagram}
            />
          </div>
        </div>
        <div className="flex flex-col items-center md:items-end">
          <h5 className="text-gray-700 mb-4 text-center">
            LISTEN ON YOUR FAVORITE PLATFORM:
          </h5>
          <div className="flex">
            <ListenLink
              href="https://itunes.apple.com/us/podcast/why-they-did-that/id1434005288"
              title="iTunes Podcasts"
              img={itunesPodcastsLogo}
            />
            <ListenLink
              href="https://open.spotify.com/show/1FdElWb8fAlmvPxy59e3uv"
              title="Spotify"
              img={spotifyLogo}
            />
            <ListenLink
              href="https://www.stitcher.com/podcast/why-they-did-that"
              title="Stitcher"
              img={stitcherLogo}
            />
          </div>
        </div>
      </div>
    </footer>
  )
}

export default Footer

const SocialIcon = ({ title, href, src, className }) => {
  return (
    <LinkIcon
      title={title}
      href={href}
      src={src}
      className={clsx(
        "h-8 mx-2 text-gray-700 hover:text-blue-500 transition-010",
        className
      )}
    />
  )
}

const ListenLink = ({ href, img, title }) => {
  return (
    <a
      href={href}
      data-toggle="tooltip"
      title="iTunes Podcasts"
      target="_blank"
      className="ml-4"
    >
      <img style={{ height: "50px", width: "50px" }} src={img} alt={title} />
    </a>
  )
}
