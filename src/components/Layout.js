import React from "react"
import PropTypes from "prop-types"
import { StaticQuery, graphql } from "gatsby"

import "typeface-work-sans"

import Header from "./Header"
import Footer from "./Footer"

export const globalPadding = `px-10 sm:px-20`
export const globalMargin = `mx-10 sm:mx-20`

function Layout({ bg = () => null, children, slimHeader, floatHeader }) {
  return (
    <StaticQuery
      query={graphql`
        query SiteTitleQuery {
          site {
            siteMetadata {
              title
            }
          }
        }
      `}
      render={data => (
        // outer container: no padding
        <div>
          {/* inner container: minimum screen height, some padding */}
          <div className="flex flex-col font-sans min-h-screen text-gray-900">
            <Header
              float={floatHeader}
              siteTitle={data.site.siteMetadata.title}
              slim={slimHeader}
            />

            {children}
          </div>
          <Footer />
        </div>
      )}
    />
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired
}

export default Layout
