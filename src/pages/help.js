import React from "react"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import clsx from "clsx"
import { useForm } from "@statickit/react"

import { SubmitButton, Input } from "../elements/forms"
import Layout, { globalPadding } from "../components/Layout"
import SEO from "../components/seo"

export const query = graphql`
  {
    coverImage: file(relativePath: { eq: "drp-banner.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 1152) {
          ...GatsbyImageSharpFluid_withWebp_noBase64
        }
      }
    }
  }
`

const ContactPage = ({ data }) => {
  return (
    <Layout>
      <SEO title="Help" keywords={[`help`, `volunteer`]} />

      <div
        className={clsx(globalPadding, "flex flex-col items-center mb-12 mt-6")}
      >
        <div className="w-full max-w-6xl mb-10">
          <Img fluid={data.coverImage.childImageSharp.fluid} className="" />
        </div>
        <div className="max-w-3xl">
          <h1 className="text-5xl font-semibold">Collaborate With Us</h1>
          <p className="mb-8">
            <strong className="text-xl font-semibold">
              Utilizing your talents for others.
            </strong>
          </p>
          <p className="mb-8">
            Here at Why They Did That, we’re all about getting the message of
            the Bible to as many people as possible — but we can’t do it alone.
            We need your help!
          </p>
          <p className="mb-8">
            God has given us all gifts and talents to use for his glory, and we
            hope that our podcast can be a platform for you to share those gifts
            that He’s given to you. Are you good with Adobe Photoshop or
            After-Effects? Maybe you draw; maybe you do Calligraphy; maybe you
            sing or play an instrument; or maybe its something else entirely!
            Whatever it is, we’d love to hear how you feel you can add to this
            ministry. Fill out this brief form and we’ll get back to you pronto!
          </p>
          <p className="mb-8">
            Perhaps God has blessed you financially and you’d love to assist The
            “Why They Did That” Podcast with our mission, you can help too by
            giving a donation or sponsoring an episode.
          </p>
          <HelpForm />
        </div>
      </div>
    </Layout>
  )
}

const HelpForm = () => {
  const [state, submit] = useForm("08380ed650d4")

  if (state.succeeded) {
    return (
      <div className="text-green-700 text-lg font-semibold">
        Awesome, we'll be in touch soon!
      </div>
    )
  }

  return (
    <form
      name="Help Us"
      className="flex flex-col items-start"
      onSubmit={submit}
    >
      <p className="leading-loose mb-8">
        <strong className="text-xl font-semibold leading-tight">
          Fill out the following form if you would like to make any of your
          time, talents, or resources available to help carry forward the
          mission of Why They Did That.
        </strong>
      </p>

      <Input
        id="name"
        label="Name"
        placeholder="Mr. Awesome"
        validationErrors={state.errors}
      />

      <Input
        id="email"
        label="Email Address"
        placeholder="you@example.com"
        validationErrors={state.errors}
      />

      <Input
        id="message"
        label="Message"
        placeholder="How can you help?"
        validationErrors={state.errors}
        input={props => (
          <textarea
            className="appearance-none bg-gray-200 mb-6 px-3 py-2 rounded-md text-gray-700 w-full"
            rows="8"
            {...props}
          />
        )}
      />

      <SubmitButton>
        {state.submitting ? "Sending..." : "Send Message"}
      </SubmitButton>
    </form>
  )
}

export default ContactPage
