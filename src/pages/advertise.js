import React from "react"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import clsx from "clsx"
import { useForm } from "@statickit/react"

import Layout, { globalPadding } from "../components/Layout"
import SEO from "../components/seo"
import { Input, SubmitButton } from "../elements/forms"

export const query = graphql`
  {
    coverImage: file(relativePath: { eq: "drp-banner.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 1152) {
          ...GatsbyImageSharpFluid_withWebp_noBase64
        }
      }
    }
  }
`

const ContactPage = ({ data }) => {
  return (
    <Layout>
      <SEO title="Advertise" keywords={[`advertise`, `ad`, `sponsor`]} />

      <div
        className={clsx(globalPadding, "flex flex-col items-center mb-12 mt-6")}
      >
        <div className="w-full max-w-6xl mb-10">
          <Img fluid={data.coverImage.childImageSharp.fluid} className="" />
        </div>
        <div className="max-w-3xl w-full">
          <h1 className="text-5xl font-semibold">Sponsorship & Advertising</h1>
          <p className="mb-8">
            <strong className="text-xl font-semibold">
              Reaching others. Together.
            </strong>
          </p>
          <p className="mb-8">
            Why They Did That has been blessed to see huge success and rapid
            growth since our inception in August of 2018. We’ve been able to
            reach thousands of young people around the world with a deep,
            relevant, and personal Biblical message. It’s exciting to imagine
            what God has in store for this ministry, but also for yours! In
            addition to our own content, we desire to use this show as a means
            of connecting our audience with your resources. If you desire the
            same, please fill out the form below and we’ll get back to you with
            the various sponsorship opportunities we have available.
          </p>
          {/* <h2 className="text-3xl font-semibold">How does it work?</h2>
          <p className="mb-8">
            Currently we offer three options for sponsorship. Descriptions and
            pricing for each are listed down below:
          </p>
          <div className="su-table su-table-alternate mb-12 w-full overflow-scroll">
            <p />
            <table>
              <tbody>
                <tr>
                  <td className="text-center">
                    <strong>Sponsorship</strong>
                  </td>
                  <td className="text-center">
                    <strong>Exposure</strong>
                  </td>
                  <td className="text-center">
                    <strong>Pricing</strong>
                  </td>
                </tr>
                <tr>
                  <td className="text-left">
                    <em>Level 1</em>
                  </td>
                  <td className="text-left">
                    <ul>
                      <li>
                        Verbal mention at end of episode (“Made possible by
                        ______” “Thanks to ______”)
                      </li>
                    </ul>
                  </td>
                  <td className="text-left">$5/episode</td>
                </tr>
                <tr>
                  <td>
                    <em>Level 2</em>
                  </td>
                  <td>
                    <ul>
                      <li>
                        Written description of your business/ministry in our
                        show notes and on our website, plus a link to your
                        website.
                      </li>
                      <li>Includes Level 1 Benefits</li>
                    </ul>
                  </td>
                  <td>$10/episode</td>
                </tr>
                <tr>
                  <td className="text-left">
                    <em>Level 3</em>
                  </td>
                  <td className="text-left">
                    <ul>
                      <li>
                        Secure our 30-second advertisement slot describing your
                        offering, plus a personalized link that directs
                        listeners to your online content.
                      </li>
                      <li>Includes Level 1 &amp; 2 Benefits</li>
                    </ul>
                  </td>
                  <td className="text-left">
                    $25/episode + additional $10 per 1,000 downloads
                  </td>
                </tr>
              </tbody>
            </table>
            <p />
          </div> */}
          <AdvertiseForm />
        </div>
      </div>
    </Layout>
  )
}

const AdvertiseForm = () => {
  const [state, submit] = useForm("ee2353b075f7")

  if (state.succeeded) {
    return (
      <div className="text-green-700 text-lg font-semibold">
        Awesome, we'll be in touch soon!
      </div>
    )
  }

  return (
    <form
      name="Advertise"
      className="flex flex-col items-start"
      onSubmit={submit}
    >
      <p className="leading-loose mb-8">
        <strong className="text-2xl font-semibold leading-tight">
          Interested in sponsoring an episode? Fill out the following
          application.
        </strong>
      </p>

      <Input
        id="name"
        label="Name"
        placeholder="Mr. Moneybags"
        validationErrors={state.errors}
      />

      <Input
        id="email"
        label="Email Address"
        placeholder="cha-ching@company.com"
        validationErrors={state.errors}
      />

      <Input
        id="message"
        label="Message"
        placeholder="What would you like to advertise?"
        validationErrors={state.errors}
        input={props => (
          <textarea
            className="appearance-none bg-gray-200 mb-6 px-3 py-2 rounded-md text-gray-700 w-full"
            rows="8"
            {...props}
          />
        )}
      />

      <SubmitButton>
        {state.submitting ? "Sending..." : "Send Message"}
      </SubmitButton>
    </form>
  )
}

export default ContactPage
