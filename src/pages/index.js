// TODO: I would like to try out sanity.io and other providers
import React from "react"

import Layout from "../components/Layout"
import SEO from "../components/seo"
import Splash from "../pageSections/home/Splash"
import AllEpisodes from "../pageSections/home/AllEpisodes"
import Quotes from "../pageSections/home/Quotes"
import HelpUs from "../pageSections/home/HelpUs"

const IndexPage = () => {
  return (
    <Layout slimHeader floatHeader>
      <SEO title="Home" keywords={[`podcast`, `bible`, `wtdt`]} />
      <Splash />
      <AllEpisodes />
      <Quotes />
      <HelpUs />
    </Layout>
  )
}

export default IndexPage
