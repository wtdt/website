import React from "react"
import clsx from "clsx"
import { graphql } from "gatsby"
import Img from "gatsby-image/withIEPolyfill"
import SEO from "../components/seo"
import Link from "gatsby-link"
import { linkResolver } from "../utils/linkResolver"

// import {
//   facebook,
//   twitter,
//   instagram,
//   globe,
//   linkedin,
//   LinkIcon
// } from "../elements/icons"
import Layout, { globalPadding } from "../components/Layout"

const Team = ({ data }) => {
  let teamMembers = data.teamMembers.edges

  return (
    <Layout>
      <SEO
        title="Team"
        keywords={[`team`, `employees`, `volunteers`, `members`]}
      />
      <div
        className={clsx(
          "flex flex-col items-center pt-16 pb-16",
          globalPadding
        )}
      >
        <div className={clsx("max-w-6xl w-full")}>
          <h1 className="font-semibold text-5xl mb-10 ">Meet the Team</h1>
          <div className="flex justify-start items-start -m-4 flex-wrap">
            {teamMembers.map(({ node }) => (
              <TeamMember node={node} key={node.uid} />
            ))}
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default Team

const TeamMember = ({ node }) => {
  return (
    <Link
      className="h-132 max-w-lg w-full md:w-1/2 lg:w-1/3 p-4"
      to={linkResolver(node)}
    >
      <div className="flex flex-col relative hover-parent overflow-hidden h-full border-4 border-transparent hover:border-blue-wtdt">
        {/* the background image */}
        {node.data.picture.localFile ? (
          <Img
            objectPosition="center top"
            className="w-full h-full object-cover object-center"
            fluid={node.data.picture.localFile.childImageSharp.fluid}
            alt={node.data.picture.alt}
          />
        ) : (
          <div className="w-full h-full bg-yellow-450" />
        )}
        {/* the bio text (only shows on hover) */}
        {/* <div className="absolute bottom-0 right-0 w-full h-full bg-white opacity-0 parent-hover:opacity-1 transition-050">
          <div
            className="text-gray-700 overflow-hidden relative m-8"
            style={{ maxHeight: 1.5 * 11 + "rem" }}
          >
            <div dangerouslySetInnerHTML={{ __html: node.data.bio.html }} />
            <div
              className="right-0 absolute bg-white text-blue-500"
              style={{ bottom: "0rem", boxShadow: "white 0 0 25px 5px" }}
            >
               Read More
            </div>
          </div>
        </div> */}

        {/* the name and links chunk */}
        <div className="w-3/5 min-w-48 max-w-full absolute bottom-0 right-0 bg-white">
          <div className="p-4 flex flex-col items-end">
            <h4 className="text-sm text-right">
              {node.data.role.text.toUpperCase()}
            </h4>
            <h3 className="pl-4 leading-tight font-semibold text-2xl flex text-right">
              {node.data.name.text}
            </h3>
            {/* <div className="w-16 border-b border-blue-wtdt lg:opacity-0 parent-hover:opacity-1 transition-050 mb-4" /> */}
            {/* <div className="flex mb-2" onClick={e => e.stopPropagation()}>
              <Link className="text-blue-500" to={linkResolver(node)}>
                More about Dean
              </Link>
            </div> */}
          </div>
        </div>

        {/* the blue border (only shows on hover) */}
        {/* <div className="absolute bottom-0 right-0 w-full h-full border-4 border-blue-wtdt opacity-0 parent-hover:opacity-1 transition-050 pointer-events-none" /> */}
      </div>
    </Link>
  )
}

// const SocialIcon = ({ title, href, src, className }) => {
//   return (
//     <LinkIcon
//       title={title}
//       href={href}
//       src={src}
//       className={clsx(
//         "h-6 ml-2 text-gray-800 hover:text-blue-500 transition-050",
//         className
//       )}
//     />
//   )
// }

export const query = graphql`
  {
    teamMembers: allPrismicTeamMember(sort: { fields: data___sort_order }) {
      edges {
        node {
          uid
          type
          data {
            name {
              html
              text
            }
            role {
              html
              text
            }
            bio {
              html
              text
            }
            picture {
              alt
              copyright
              url
              localFile {
                childImageSharp {
                  fluid(maxWidth: 480) {
                    ...GatsbyImageSharpFluid_withWebp
                  }
                }
              }
            }
            linkedin {
              link_type
              url
              target
            }
            instagram {
              link_type
              url
              target
            }
            personal_website {
              link_type
              url
              target
            }
            facebook_page {
              link_type
              url
              target
            }
            twitter {
              link_type
              url
              target
            }
          }
        }
      }
    }
  }
`
