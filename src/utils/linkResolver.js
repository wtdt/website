exports.linkResolver = docMeta => {
  let typeSegment = docMeta.type
  if (docMeta.type == "team_member") {
    typeSegment = "team"
  }

  return `/${typeSegment}/${docMeta.uid}`
}

exports.resolveLink = ({ node, key, value }) => docMeta => {
  return exports.linkResolver(docMeta)
}
