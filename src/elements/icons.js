import React from "react"
import clsx from "clsx"

export const LinkIcon = ({ href, title, className, src }) => {
  return (
    <a
      href={href}
      rel="nofollow"
      title={title}
      data-toggle="tooltip"
      data-placement="top"
      target="_blank"
    >
      <Icon className={className} src={src} />
    </a>
  )
}

export const Icon = ({ className, src }) => {
  const SvgComponent = src

  return <SvgComponent className={clsx("fill-current", className)} />
}

export const facebook = ({ ...props }) => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" {...props}>
    <path d="M427 64H85c-11 0-21 10-21 21v342c0 12 10 21 21 21h171V296h-46v-56h46v-41c0-50 34-77 79-77l49 2v52h-35c-24 0-29 11-29 28v36h57l-7 56h-50v152h107c12 0 21-9 21-21V85c0-12-9-21-21-21z" />
  </svg>
)

export const twitter = ({ ...props }) => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" {...props}>
    <path d="M492 110c-17 7-36 12-56 15 20-12 36-31 43-54-19 11-39 19-61 24a97 97 0 0 0-165 88c-81-4-152-43-200-101a97 97 0 0 0 30 129c-16 0-31-5-44-12v1c0 47 33 86 78 95a97 97 0 0 1-44 2c12 38 48 66 91 67a194 194 0 0 1-144 40 274 274 0 0 0 424-245c19-13 35-30 48-49z" />
  </svg>
)

export const instagram = ({ ...props }) => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 44 44" {...props}>
    <path d="M36.6 6H7.4C6.6 6 6 6.6 6 7.4v29.2c0 .8.6 1.4 1.4 1.4h29.2c.8 0 1.4-.6 1.4-1.4V7.4c0-.8-.6-1.4-1.4-1.4zm-9 16a5.6 5.6 0 1 1-11.2 0 5.6 5.6 0 0 1 11.2 0zM34 34H10V20h2.6A9.6 9.6 0 0 0 22 31.6 9.6 9.6 0 0 0 31.4 20H34v14zm0-18h-6v-6h6v6z" />
  </svg>
)

export const globe = ({ ...props }) => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" {...props}>
    <path d="M256 48h-1A207 207 0 0 0 48 256a207 207 0 0 0 207 208h1a208 208 0 0 0 0-416zm180 194h-77c-1-27-5-52-10-76 17-6 34-13 50-21 22 28 35 61 37 97zm-194 0h-62c1-24 4-48 9-69 17 4 35 6 53 7v62zm0 28v62c-18 1-36 3-53 7-5-21-8-45-9-69h62zm28 0h61c0 24-3 48-8 69-18-4-35-6-53-7v-62zm0-28v-62c18-1 35-3 53-7 5 21 8 45 8 69h-61zm109-118l-38 16c-7-22-16-40-26-55 24 8 46 22 64 39zm-64 23l-45 6V79c17 10 34 34 45 68zm-73-68v74c-15-1-31-3-45-6 11-34 28-59 45-68zm-46 6c-10 15-19 33-26 55l-37-16c18-17 40-30 63-39zm-82 60c15 8 32 15 49 21-6 24-9 49-10 76H76c2-35 15-69 37-97zM76 270h77c1 27 4 52 10 76-17 6-34 13-50 21-21-28-34-61-37-97zm57 118l37-16c7 22 16 40 26 55-23-9-45-22-63-39zm64-23c14-3 30-5 45-6v74c-17-9-34-34-45-68zm73 68v-74l45 6c-11 34-28 58-45 68zm45-6c10-15 19-33 27-55l38 16c-19 17-41 31-65 39zm84-60c-16-8-33-15-50-21 5-24 9-49 10-76h77c-2 36-15 69-37 97z" />
  </svg>
)

export const linkedin = ({ ...props }) => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" {...props}>
    <path d="M417 64H97c-18 0-33 13-33 30v321c0 17 15 33 33 33h320c18 0 31-16 31-33V94c0-17-13-30-31-30zM183 384h-55V213h55v171zm-26-197c-18 0-29-13-29-29 0-17 12-30 30-30s29 13 29 30c0 16-11 29-30 29zm227 197h-55v-93c0-23-8-38-28-38-15 0-24 10-28 20-2 4-2 9-2 14v97h-55V213h55v24c8-12 21-28 50-28 36 0 63 24 63 75v100z" />
  </svg>
)

export const backChevron = ({ ...props }) => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" {...props}>
    <path d="M217.9 256L345 129c9.4-9.4 9.4-24.6 0-33.9-9.4-9.4-24.6-9.3-34 0L167 239c-9.1 9.1-9.3 23.7-.7 33.1L310.9 417c4.7 4.7 10.9 7 17 7s12.3-2.3 17-7c9.4-9.4 9.4-24.6 0-33.9L217.9 256z" />
  </svg>
)
