import React from "react"
import { ValidationError } from "@statickit/react"

export const NetlifyForm = ({ children, name, className, action }) => {
  return (
    <form
      name={name}
      data-netlify="true"
      method="POST"
      className={className}
      action={action}
    >
      <input type="hidden" name="form-name" value={name} />
      {children}
    </form>
  )
}

export const SubmitButton = ({ children, ...props }) => {
  return (
    <button
      className="bg-blue-wtdt hover:bg-blue-500 px-4 py-2 text-lg text-white"
      {...props}
      type="submit"
    >
      {children}
    </button>
  )
}

const defaultInput = props => (
  <input
    className="appearance-none block bg-gray-200 mb-6 px-3 py-2 rounded-md text-gray-700 w-full"
    type="text"
    {...props}
  />
)

export const Input = ({ id, label, placeholder, input, validationErrors }) => {
  const Input = input || defaultInput

  return (
    <>
      <label
        className="block font-semibold mb-2 text-sm uppercase"
        htmlFor={id}
      >
        {label}
      </label>

      <ValidationError
        className="text-red-700 mb-1 -mt-2"
        field={id}
        prefix={label}
        errors={validationErrors}
      />

      <Input id={id} placeholder={placeholder} name={id} />
    </>
  )
}
