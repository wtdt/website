const { colors, spacing } = require("tailwindcss/defaultTheme")

const allSpacing = {
  ...spacing,
  "68": "17rem",
  "80": "20rem",
  "92": "23rem",
  "120": "30rem",
  "132": "33rem"
}

module.exports = {
  variants: {
    // borderWidth: ["responsive", "hover"]
    opacity: ["responsive", "hover"]
  },
  theme: {
    extend: {
      colors: {
        yellow: {
          ...colors.yellow,
          "450": "#eed348"
        },
        blue: {
          ...colors.blue,
          wtdt: "#318CCC"
        }
      },
      zIndex: {
        "-1": "-1"
      },
      spacing: {
        "68": "17rem",
        "80": "20rem",
        "92": "23rem",
        "120": "30rem",
        "132": "33rem"
      },
      minWidth: {
        ...allSpacing
      },
      maxHeight: {
        ...allSpacing
      }
      // maxWidth: {
      //   ...spacing
      // }
    },

    fontFamily: {
      sans: [
        '"Work Sans"',
        "-apple-system",
        "BlinkMacSystemFont",
        '"Segoe UI"',
        "Roboto",
        '"Helvetica Neue"',
        "Arial",
        '"Noto Sans"',
        "sans-serif",
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
        '"Noto Color Emoji"'
      ],
      // "sans-alt": [
      //   "Overpass",
      //   "-apple-system",
      //   "BlinkMacSystemFont",
      //   '"Segoe UI"',
      //   "Roboto",
      //   '"Helvetica Neue"',
      //   "Arial",
      //   '"Noto Sans"',
      //   "sans-serif",
      //   '"Apple Color Emoji"',
      //   '"Segoe UI Emoji"',
      //   '"Segoe UI Symbol"',
      //   '"Noto Color Emoji"'
      // ],
      serif: [
        "Libre Baskerville",
        "Georgia",
        "Cambria",
        '"Times New Roman"',
        "Times",
        "serif"
      ],
      mono: [
        "Menlo",
        "Monaco",
        "Consolas",
        '"Liberation Mono"',
        '"Courier New"',
        "monospace"
      ]
    }
  },
  plugins: []
}
