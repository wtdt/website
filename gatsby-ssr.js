import React from "react"

export const onRenderBody = ({ setPostBodyComponents }) => {
  const components = [
    <script
      key="prismic-script"
      type="text/javascript"
      src="//static.cdn.prismic.io/prismic.min.js"
    />
  ]

  setPostBodyComponents(components)
}
